#!/usr/bin/env bats

setup() {
  echo "Setup happens before each test."
  echo "DOCKER_HOST: $DOCKER_HOST"
  echo "BITBUCKET_DOCKER_HOST_INTERNAL: $BITBUCKET_DOCKER_HOST_INTERNAL"

  PIPE_IMAGE=${DOCKER_IMAGE:="test/snyk-pipe"}
  echo "PIPE_IMAGE: $PIPE_IMAGE"

  run docker build -t ${PIPE_IMAGE} .
}

teardown() {
    echo "Teardown happens after each test."
}

@test "Tests and fails on docker app" {
  local IMAGE_NAME="docker-goof"
  docker build -t $IMAGE_NAME test/fixtures/docker

  run_pipe_container test/fixtures/docker \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="docker" \
    --env=IMAGE_NAME="$IMAGE_NAME" \
    --env=TARGET_FILE="Dockerfile"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "package manager" | grep -i "deb"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Tests and fails on docker app for JSON ouput" {
  local IMAGE_NAME="docker-goof"
  docker build -t $IMAGE_NAME test/fixtures/docker

  run_pipe_container test/fixtures/docker \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="docker" \
    --env=IMAGE_NAME="$IMAGE_NAME" \
    --env=TARGET_FILE="Dockerfile" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "packageManager" | grep -i "deb"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Fails on app with vulns" {
  run_pipe_container test/fixtures/npm \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="node" \

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "known issues" | grep -i "vulnerable path"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Fails on app with vulns with manually set API URL" {
  run_pipe_container test/fixtures/npm \
    --env=SNYK_TOKEN="$SNYK_EU_TOKEN" \
    --env=LANGUAGE="node" \
    --env=SNYK_API="https://api.eu.snyk.io"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "known issues" | grep -i "vulnerable path"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Fails on app with vulns and generate Code Insight report" {
  run_pipe_container test/fixtures/npm \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="node" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "uniqueCount"
  echo $output | grep -i "vulnerable dependency path"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Fails on app with vulns including critical and generates a Code Insight report" {
  run_pipe_container test/fixtures/npm-with-critical \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="node" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "uniqueCount"
  echo $output | grep -i "vulnerable dependency path"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Fails on app with vulns including critical when SEVERITY_THRESHOLD is set to critical" {
  run_pipe_container test/fixtures/npm-with-critical \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="node" \
    --env=SEVERITY_THRESHOLD="critical" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "uniqueCount"
  echo $output | grep -i "vulnerable dependency path"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Passes on app with vulns but with threshold" {
  run_pipe_container test/fixtures/npm \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="node" \
    --env=SEVERITY_THRESHOLD="high"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i "known issues" | grep -i "no vulnerable path"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Passes on app with vulns but with threshold and generate Code Insight report" {
  run_pipe_container test/fixtures/npm \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="node" \
    --env=SEVERITY_THRESHOLD="high" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i "uniqueCount"
  echo $output | grep -i "no high or critical severity vulnerabilities"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Passes on app with vulns but with dont_break_build" {
  run_pipe_container test/fixtures/npm \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="node" \
    --env=DONT_BREAK_BUILD="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i "known issues" | grep -i "vulnerable path"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Passes on app with vulns but with dont_break_build and generate Code Insight report" {
  run_pipe_container test/fixtures/npm \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="node" \
    --env=DONT_BREAK_BUILD="true" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i "uniqueCount"
  echo $output | grep -i "vulnerable dependency path"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Protect skips succesfully if no policy file" {
  run_pipe_container test/fixtures/composer \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="composer" \
    --env=DONT_BREAK_BUILD="true" \
    --env=PROTECT="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Monitor creates a snapshot" {
  run_pipe_container test/fixtures/npm \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="node" \
    --env=DONT_BREAK_BUILD="true" \
    --env=MONITOR="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i "Explore this snapshot"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Tests and fails on Node.js app" {
  run_pipe_container test/fixtures/npm \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="node"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "package manager" | grep -i "npm"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Tests and fails on Node.js app and generate Code Insight report" {
  run_pipe_container test/fixtures/npm \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="node" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "packageManager" | grep -i "npm"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Tests and fails on PHP app" {
  run_pipe_container test/fixtures/composer \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="composer"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "package manager" | grep -i "composer"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Tests and fails on PHP app and generate Code Insight report" {
  run_pipe_container test/fixtures/composer \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="composer" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "packageManager" | grep -i "composer"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Tests and fails on Ruby app" {
  run_pipe_container test/fixtures/rubygems \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="ruby"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "package manager" | grep -i "rubygems"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Tests and fails on Ruby app and generate Code Insight report" {
  run_pipe_container test/fixtures/rubygems \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="ruby" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "packageManager" | grep -i "rubygems"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Tests and fails on dotnetcore app" {
  run_pipe_container test/fixtures/nuget \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="dotnet" \
    --env=TARGET_FILE="DotNetCoreTest.sln"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "package manager" | grep -i "nuget"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Tests and fails on dotnetcore app and generate Code Insight report" {
  run_pipe_container test/fixtures/nuget \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    --env=LANGUAGE="dotnet" \
    --env=TARGET_FILE="DotNetCoreTest.sln" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "packageManager" | grep -i "nuget"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

run_pipe_container() {
  echo "Running pipe container..."
  FIXTURE_DIR="$(pwd)/$1"

  echo "DOCKER_HOST: $DOCKER_HOST"
  echo "BITBUCKET_DOCKER_HOST_INTERNAL: $BITBUCKET_DOCKER_HOST_INTERNAL"

  run docker container run \
    --volume=/usr/local/bin/docker:/usr/local/bin/docker:ro \
    --volume=/var/run/docker.sock:/var/run/docker.sock \
    --env=DOCKER_HOST="tcp://$BITBUCKET_DOCKER_HOST_INTERNAL:2375" \
    --add-host="host.docker.internal:$BITBUCKET_DOCKER_HOST_INTERNAL" \
    --env=BITBUCKET_DOCKER_HOST_INTERNAL="$BITBUCKET_DOCKER_HOST_INTERNAL" \
    --workdir=$FIXTURE_DIR \
    --volume=$FIXTURE_DIR:$FIXTURE_DIR \
    --env=BITBUCKET_CLONE_DIR="$FIXTURE_DIR" \
    --env=BITBUCKET_REPO_OWNER="snyk" \
    --env=BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
    --env=BITBUCKET_COMMIT="$BITBUCKET_COMMIT" \
    --env=BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
    ${@:2} \
    ${PIPE_IMAGE}
}
