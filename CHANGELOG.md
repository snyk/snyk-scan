# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.2

- patch: fix: Security upgrade axios from 1.6.7 to 1.7.4

## 1.0.1

- patch: update dependencies and base image [CLI-76]

## 1.0.0

- major: feat: remove usage of deprecated Snyk CLI Docker images. 
BREAKING CHANGE: Snyk CLI Docker images are now deprecated. 
- Please use tags supported by Snyk Images for the LANGUAGE variable instead. 
- Alternately, users can provide their own base image using the SNYK_BASE_IMAGE variable.

## 0.7.0

- minor: Fix SNYK_API testing

## 0.6.0

- minor: Add support for configuring the SNYK_API

## 0.5.4

- patch: Report application vulnerabilities in Container scans.

## 0.5.3

- patch: Fix failing builds even if parameter not to fail was set

## 0.5.2

- patch: fix: don't hard code --project-name when calling snyk monitor

## 0.5.1

- patch: Update pipe's metadata according to new Bitbucket Pipes structure.

## 0.5.0

- minor: Fix security vulnerability with authentication.

## 0.4.6

- patch: Fix docker configuration

## 0.4.5

- patch: Fix DOCKER_HOST config

## 0.4.4

- patch: Update integration name

## 0.4.3

- patch: fix version bump for pipe/common.sh

## 0.4.2

- patch: Add SNYK_INTEGRATION_NAME and SNYK_INTEGRATION_VERSION env vars

## 0.4.1

- patch: Code Insight report, include Introduced and Remediation fields. Documetation improvements

## 0.4.0

- minor: Add SNYK_TEST_JSON_INPUT option to generate CI report only

## 0.3.6

- patch: Code Insight report improviments

## 0.3.5

- patch: Code improvement - unify bitbucket client to use axios

## 0.3.4

- patch: Small update in README to include new option

## 0.3.3

- patch: Include vulnerabilities details in Code Insight report and code improviments

## 0.3.2

- patch: BugFix on code insight report

## 0.3.1

- patch: Add debug logs to help track unexpected errors

## 0.3.0

- minor: Add Code Insight report option to pipe

## 0.2.0

- minor: Add Snyk Protect option to pipe

## 0.1.3

- patch: Small Changes to README.md

## 0.1.2

- patch: Add integration tests

## 0.1.1

- patch: add DEBUG support to cli execution
- patch: rename repo to snyk-scan and add logo

## 0.1.0

- minor: Initial release

