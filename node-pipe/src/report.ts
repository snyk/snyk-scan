import * as _ from "lodash";
import {
  ReportDataType,
  ReportResult,
  ReportType,
  ReportHeader,
  AnnotationType,
  Severity,
  SeverityOrder
} from "./common/types";

const MAX_ISSUES_PER_REPORT = 1000;
export const SNYK_VULN_PAGE = "https://app.snyk.io/vuln";
export const LOGO_URL =
  "https://res.cloudinary.com/snyk/image/upload/f_auto,q_auto,w_48/v1468845259/logo/snyk-avatar.svg";

export const generateReport = (
  bitbucketRepoOwner: string,
  bitbucketRepoSlug: string,
  bitbucketBuildNumber: string,
  snykOutput: any
): object => {
  const issues = getIssuesList(snykOutput);
  const totalIssues = issues?.length ?? 0;
  const groupedIssues = _.groupBy(issues, "severity");
  const reportResult = totalIssues ? ReportResult.FAILED : ReportResult.PASSED;
  const reportType = "security";
  const title = `${reportType}/snyk (${getOrg(snykOutput)})`;

  return {
    type: "report",
    report_type: ReportType.SECURITY, // eslint-disable-line
    reporter: ReportHeader.SNYK,
    result: reportResult,
    logo_url: LOGO_URL, // eslint-disable-line
    title: title,
    link: createLinkUrl(
      bitbucketRepoOwner,
      bitbucketRepoSlug,
      bitbucketBuildNumber
    ),
    details: detailsContent(totalIssues),
    data: dataContent(totalIssues, groupedIssues)
  };
};

export function parseSeverityOrder(severity: string): SeverityOrder {
  const lcSev = severity.toLowerCase();
  if (lcSev === "critical") {
    return SeverityOrder.critical;
  } else if (lcSev === "high") {
    return SeverityOrder.high;
  } else if (lcSev === "medium") {
    return SeverityOrder.medium;
  } else if (lcSev === "low") {
    return SeverityOrder.low;
  } else {
    throw new Error(`invalid severity: ${severity}`);
  }
}

// Get all the behaviour from Registry
// https://github.com/snyk/registry/blob/8f5a01baf433b9e66afc3020b871eaf75187471e/frontend/src/components/IssueCardVulnerablePath.vue#L87
export const annotationDetails = issue => {
  let detail = `Snyk vulnerability ID: ${issue.id}`;
  detail += ` | Introduced through: ${getIssuePath(issue)}`;
  detail += getRemediation(issue);

  return detail;
};

export const generateAnnotations = (snykOutput: any): object => {
  const issues = getIssuesList(snykOutput);
  const targetFile = snykOutput.displayTargetFile;

  const sortedIssues = issues.sort((prev, next) => {
    const prevSev = parseSeverityOrder(prev.severity);
    const nextSev = parseSeverityOrder(next.severity);
    return prevSev - nextSev;
  });

  return sortedIssues.slice(0, MAX_ISSUES_PER_REPORT).map((issue, index) => ({
    type: "report_annotation",
    annotation_type: AnnotationType.VULNERABILITY, // eslint-disable-line
    external_id: `${index}:${issue.id}`, // eslint-disable-line
    summary: `${issue.name}@${issue.version}: ${issue.title}`,
    severity: Severity[issue.severity],
    path: targetFile,
    link: `${SNYK_VULN_PAGE}/${issue.id}`,
    details: annotationDetails(issue)
  }));
};

const getOrg = snykOutput => {
  if (!Array.isArray(snykOutput)) {
    snykOutput = [snykOutput];
  }

  return snykOutput[0]?.org ?? "";
};

const getIssuesList = (snykOutput): any => {
  if (!Array.isArray(snykOutput)) {
    snykOutput = [snykOutput];
  }

  let issues = [];
  snykOutput.forEach(output => {
    issues = issues.concat(_.uniqBy(output.vulnerabilities, "id"));

    // In the case of Snyk Container scanning, the JSON output also has an
    // 'applications' object which has separate vulnerabilities
    output.applications?.forEach(appOutput => {
      issues = issues.concat(_.uniqBy(appOutput.vulnerabilities, "id"));
    });
  });

  return issues;
};

const detailsContent = (totalIssues: number): string =>
  `This pull request introduces ${totalIssues} ${pluralizeIssueWord(
    totalIssues
  )}`;
const pluralizeIssueWord = (totalIssues: number): string =>
  totalIssues === 1 ? "issue" : "issues";

const createLinkUrl = (
  bitbucketRepoOwner,
  bitbucketRepoSlug,
  bitbucketBuildNumber
): URL =>
  new URL(
    `https://bitbucket.org/${bitbucketRepoOwner}/${bitbucketRepoSlug}/addon/pipelines/home#!/results/${bitbucketBuildNumber}`
  );
const dataContent = (totalIssues, groupedIssues) => [
  {
    title: ReportHeader.TOTAL,
    type: ReportDataType.NUMBER,
    value: totalIssues
  },
  {
    title: ReportHeader.CRITICAL_SEVERITY,
    type: ReportDataType.NUMBER,
    value: groupedIssues.critical?.length ?? 0
  },
  {
    title: ReportHeader.HIGH_SEVERITY,
    type: ReportDataType.NUMBER,
    value: groupedIssues.high?.length ?? 0
  },
  {
    title: ReportHeader.MEDIUM_SEVERITY,
    type: ReportDataType.NUMBER,
    value: groupedIssues.medium?.length ?? 0
  },
  {
    title: ReportHeader.LOW_SEVERITY,
    type: ReportDataType.NUMBER,
    value: groupedIssues.low?.length ?? 0
  }
];

const getIssuePath = issue => issue.from.join(" › ");

const isRemediable = issue => issue?.isPinnable || issue?.isUpgradable;

const getRemediation = issue => {
  if (!isRemediable(issue)) {
    return "";
  }

  let remediation = " | Remediation: ";
  // Taking precedence over isUpgradeable. This is to ensure dependencies
  // in pinnable ecosystems are displayed in the new consistent format,
  // while preserving the old format "Upgrade to $name@$version" for
  // other ecosystems.
  if (issue?.isPinnable) {
    const toVersions = issue.fixedIn.join(" or ");
    const action = issue.from.length > 2 ? "Pin" : "Upgrade";
    remediation += `${action} ${issue.name} to version ${toVersions}`;
  } else {
    if (issue?.isUpgradable) {
      if (issue?.isOutdated) {
        if (issue?.isPatchable) {
          remediation += `Run snyk wizard to patch ${issue.name}@${issue.version}.`;
        } else {
          remediation += `Your dependencies are out of date, otherwise you would be using a newer ${issue.name} than ${issue.name}@${issue.version}. Try reinstalling your dependencies. If the problem persists, one of your dependencies may be bundling outdated modules.`;
        }
      } else {
        remediation += `Upgrade to ${issue.upgradePath.find(path => !!path)}`;
      }
    }
  }

  return remediation;
};
