import * as fs from "fs";
import {
  ReportDataType,
  ReportType,
  ReportResult,
  ReportHeader
} from "../../common/types";

import {
  generateReport,
  LOGO_URL,
  SNYK_VULN_PAGE,
  generateAnnotations,
  annotationDetails
} from "../../report";

const fixturesBasePath = "./src/__tests__/fixtures";
const bitbucketRepoOwner = "snyk";
const bitbucketRepoSlug = "snyk-scan";
const bitbucketBuildNumber = "1";
const buildUrl = new URL(
  `https://bitbucket.org/${bitbucketRepoOwner}/${bitbucketRepoSlug}/addon/pipelines/home#!/results/${bitbucketBuildNumber}`
);

describe("convert snyk output to bitbucket api input", () => {
  describe("for report", () => {
    describe("when there is NO vulnerabilities", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_without_vuln.json`;
      const snykOutput: any = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );
      it("should show 0 vulnerabilities", () => {
        const bitbucketInput: object = {
          type: "report",
          report_type: ReportType.SECURITY, // eslint-disable-line
          reporter: ReportHeader.SNYK,
          result: ReportResult.PASSED,
          logo_url: LOGO_URL, // eslint-disable-line
          title: "security/snyk (Snyk)",
          link: buildUrl,
          details: "This pull request introduces 0 issues",
          data: [
            {
              title: ReportHeader.TOTAL,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.CRITICAL_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.HIGH_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.MEDIUM_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.LOW_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            }
          ]
        };
        const result: object = generateReport(
          bitbucketRepoOwner,
          bitbucketRepoSlug,
          bitbucketBuildNumber,
          snykOutput
        );

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there in 1 vulnerabilities", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_with_vuln.json`;
      const snykOutput: any = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 1 vulnerability in the report", () => {
        const bitbucketInput: object = {
          type: "report",
          report_type: ReportType.SECURITY, // eslint-disable-line
          reporter: ReportHeader.SNYK,
          result: ReportResult.FAILED,
          logo_url: LOGO_URL, // eslint-disable-line
          title: "security/snyk (Snyk)",
          link: buildUrl,
          details: "This pull request introduces 1 issue",
          data: [
            {
              title: ReportHeader.TOTAL,
              type: ReportDataType.NUMBER,
              value: 1
            },
            {
              title: ReportHeader.CRITICAL_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.HIGH_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.MEDIUM_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.LOW_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 1
            }
          ]
        };

        const result: object = generateReport(
          bitbucketRepoOwner,
          bitbucketRepoSlug,
          bitbucketBuildNumber,
          snykOutput
        );

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there is 2 vulnerabilities, but with same Snyk ID", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_with_2_vuln_but_equal_snyk_id.json`;
      const snykOutput: any = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 1 vulnerability in the report", () => {
        const bitbucketInput: object = {
          type: "report",
          report_type: ReportType.SECURITY, // eslint-disable-line
          reporter: ReportHeader.SNYK,
          result: ReportResult.FAILED,
          logo_url: LOGO_URL, // eslint-disable-line
          title: "security/snyk (Snyk)",
          link: buildUrl,
          details: "This pull request introduces 1 issue",
          data: [
            {
              title: ReportHeader.TOTAL,
              type: ReportDataType.NUMBER,
              value: 1
            },
            {
              title: ReportHeader.CRITICAL_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.HIGH_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.MEDIUM_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.LOW_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 1
            }
          ]
        };

        const result: object = generateReport(
          bitbucketRepoOwner,
          bitbucketRepoSlug,
          bitbucketBuildNumber,
          snykOutput
        );

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there are multi manifest with 2 different vulnerabilities", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_multi_projects.json`;
      const snykOutput: any = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 2 vulnerability in the report", () => {
        const bitbucketInput: object = {
          type: "report",
          report_type: ReportType.SECURITY, // eslint-disable-line
          reporter: ReportHeader.SNYK,
          result: ReportResult.FAILED,
          logo_url: LOGO_URL, // eslint-disable-line
          title: "security/snyk (demo-applications)",
          link: buildUrl,
          details: "This pull request introduces 2 issues",
          data: [
            {
              title: ReportHeader.TOTAL,
              type: ReportDataType.NUMBER,
              value: 2
            },
            {
              title: ReportHeader.CRITICAL_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.HIGH_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 2
            },
            {
              title: ReportHeader.MEDIUM_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.LOW_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            }
          ]
        };

        const result: object = generateReport(
          bitbucketRepoOwner,
          bitbucketRepoSlug,
          bitbucketBuildNumber,
          snykOutput
        );

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there are vulnerabilities which include critical level severities", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_with_critical_vuln.json`;
      const snykOutput: any = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should include critical vulnerabilities in the report", () => {
        const bitbucketInput: object = {
          type: "report",
          report_type: ReportType.SECURITY, // eslint-disable-line
          reporter: ReportHeader.SNYK,
          result: ReportResult.FAILED,
          logo_url: LOGO_URL, // eslint-disable-line
          title: "security/snyk ()",
          link: buildUrl,
          details: "This pull request introduces 2 issues",
          data: [
            {
              title: ReportHeader.TOTAL,
              type: ReportDataType.NUMBER,
              value: 2
            },
            {
              title: ReportHeader.CRITICAL_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 1
            },
            {
              title: ReportHeader.HIGH_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 1
            },
            {
              title: ReportHeader.MEDIUM_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.LOW_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            }
          ]
        };

        const result: object = generateReport(
          bitbucketRepoOwner,
          bitbucketRepoSlug,
          bitbucketBuildNumber,
          snykOutput
        );

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there are application and container vulnerabilities in a container which include high level severities", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_container_output_with_high_vuln.json`;
      const snykOutput: any = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should include high vulnerabilities in the report", () => {
        const bitbucketInput: object = {
          type: "report",
          report_type: ReportType.SECURITY, // eslint-disable-line
          reporter: ReportHeader.SNYK,
          result: ReportResult.FAILED,
          logo_url: LOGO_URL, // eslint-disable-line
          title: "security/snyk ()",
          link: buildUrl,
          details: "This pull request introduces 4 issues",
          data: [
            {
              title: ReportHeader.TOTAL,
              type: ReportDataType.NUMBER,
              value: 4
            },
            {
              title: ReportHeader.CRITICAL_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.HIGH_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 3
            },
            {
              title: ReportHeader.MEDIUM_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 1
            },
            {
              title: ReportHeader.LOW_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            }
          ]
        };

        const result: object = generateReport(
          bitbucketRepoOwner,
          bitbucketRepoSlug,
          bitbucketBuildNumber,
          snykOutput
        );

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there are just application vulnerabilities in a container", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_container_output_with_only_app_vuln.json`;
      const snykOutput: any = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should include vulnerabilities in the report", () => {
        const bitbucketInput: object = {
          type: "report",
          report_type: ReportType.SECURITY, // eslint-disable-line
          reporter: ReportHeader.SNYK,
          result: ReportResult.FAILED,
          logo_url: LOGO_URL, // eslint-disable-line
          title: "security/snyk ()",
          link: buildUrl,
          details: "This pull request introduces 2 issues",
          data: [
            {
              title: ReportHeader.TOTAL,
              type: ReportDataType.NUMBER,
              value: 2
            },
            {
              title: ReportHeader.CRITICAL_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.HIGH_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 2
            },
            {
              title: ReportHeader.MEDIUM_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.LOW_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            }
          ]
        };

        const result: object = generateReport(
          bitbucketRepoOwner,
          bitbucketRepoSlug,
          bitbucketBuildNumber,
          snykOutput
        );

        expect(result).toEqual(bitbucketInput);
      });
    });
  });

  describe("for annotations", () => {
    describe("when there are NO vulnerabilities", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_without_vuln.json`;
      const snykOutput: object = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 0 vulnerability in the annotation list", () => {
        const bitbucketInput: Array<object> = [];

        const result: object = generateAnnotations(snykOutput);

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there is 1 vulnerability", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_with_vuln.json`;
      const snykOutput: object = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 1 vulnerability in the annotation list", () => {
        const issue = snykOutput["vulnerabilities"][0];
        const bitbucketInput: Array<object> = [
          {
            type: "report_annotation",
            annotation_type: "VULNERABILITY", // eslint-disable-line
            external_id: `0:${issue.id}`, // eslint-disable-line
            summary: `${issue.name}@${issue.version}: ${issue.title}`,
            severity: "LOW",
            path: snykOutput["displayTargetFile"],
            link: `${SNYK_VULN_PAGE}/${issue.id}`,
            details: `Snyk vulnerability ID: ${issue.id} | Introduced through: npm-fixture@1.0.0 › ircdkit@1.0.3 | Remediation: Upgrade to ircdkit@1.0.4`
          }
        ];

        const result: object = generateAnnotations(snykOutput);

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there are 2 vulnerabilities, but with same Snyk ID", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_with_2_vuln_but_equal_snyk_id.json`;
      const snykOutput: object = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 1 vulnerability in the annotation list", () => {
        const issue = snykOutput["vulnerabilities"][0];
        const bitbucketInput: Array<object> = [
          {
            type: "report_annotation",
            annotation_type: "VULNERABILITY", // eslint-disable-line
            external_id: `0:${issue.id}`, // eslint-disable-line
            summary: `${issue.name}@${issue.version}: ${issue.title}`,
            severity: "LOW",
            path: snykOutput["displayTargetFile"],
            link: `${SNYK_VULN_PAGE}/${issue.id}`,
            details: `Snyk vulnerability ID: ${issue.id} | Introduced through: npm-fixture@1.0.0 › ircdkit@1.0.3 | Remediation: Upgrade to ircdkit@1.0.4`
          }
        ];

        const result: object = generateAnnotations(snykOutput);

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there are multi manifest with 2 different vulnerabilities", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_multi_projects.json`;
      const snykOutput: any = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 2 vulnerability in the report", () => {
        const issue1 = snykOutput[0]["vulnerabilities"][0];
        const issue2 = snykOutput[1]["vulnerabilities"][0];
        const bitbucketInput: Array<object> = [
          {
            type: "report_annotation",
            annotation_type: "VULNERABILITY", // eslint-disable-line
            external_id: `0:${issue1.id}`, // eslint-disable-line
            summary: `${issue1.name}@${issue1.version}: ${issue1.title}`,
            severity: "HIGH",
            path: snykOutput["displayTargetFile"],
            link: `${SNYK_VULN_PAGE}/${issue1.id}`,
            details: `Snyk vulnerability ID: ${issue1.id} | Introduced through: io.github.snyk:todolist-mvc@1.0-SNAPSHOT › io.github.snyk:todolist-web-struts@1.0-SNAPSHOT › io.github.snyk:todolist-web-common@1.0-SNAPSHOT › io.github.snyk:todolist-core@1.0-SNAPSHOT › c3p0:c3p0@0.9.1.2`
          },
          {
            type: "report_annotation",
            annotation_type: "VULNERABILITY", // eslint-disable-line
            external_id: `1:${issue2.id}`, // eslint-disable-line
            summary: `${issue2.name}@${issue2.version}: ${issue2.title}`,
            severity: "HIGH",
            path: snykOutput["displayTargetFile"],
            link: `${SNYK_VULN_PAGE}/${issue2.id}`,
            details: `Snyk vulnerability ID: ${issue2.id} | Introduced through: io.github.snyk:todolist-core@1.0-SNAPSHOT › c3p0:c3p0@0.9.1.2`
          }
        ];

        const result: object = generateAnnotations(snykOutput);

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there are 2 vulnerabilities including critical severity level", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_with_critical_vuln.json`;
      const snykOutput: object = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("creates annotations details including critical level severity ", () => {
        const issueCritical = snykOutput["vulnerabilities"][0];
        const issueHigh = snykOutput["vulnerabilities"][1];
        const bitbucketInput: Array<object> = [
          {
            type: "report_annotation",
            annotation_type: "VULNERABILITY", // eslint-disable-line
            external_id: `0:${issueCritical.id}`, // eslint-disable-line
            summary: `${issueCritical.name}@${issueCritical.version}: ${issueCritical.title}`,
            severity: "CRITICAL",
            path: snykOutput["displayTargetFile"],
            link: `${SNYK_VULN_PAGE}/${issueCritical.id}`,
            details: `Snyk vulnerability ID: ${issueCritical.id} | Introduced through: base-3@1.0.0 › lodash@4.17.19 | Remediation: Upgrade to lodash@4.17.20`
          },
          {
            type: "report_annotation",
            annotation_type: "VULNERABILITY", // eslint-disable-line
            external_id: `1:${issueHigh.id}`, // eslint-disable-line
            summary: `${issueHigh.name}@${issueHigh.version}: ${issueHigh.title}`,
            severity: "HIGH",
            path: snykOutput["displayTargetFile"],
            link: `${SNYK_VULN_PAGE}/${issueHigh.id}`,
            details: `Snyk vulnerability ID: ${issueHigh.id} | Introduced through: base-3@1.0.0 › lodash@4.17.19 | Remediation: Upgrade to lodash@4.17.21`
          }
        ];

        const result: object = generateAnnotations(snykOutput);

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there is remediation", () => {
      describe("when is Pinnalbe", () => {
        it("should Pin because from field is greater equal than 2", () => {
          const snykOutputFilePath = `${fixturesBasePath}/snyk_issue_pinnable_but_long_from.json`;
          const issue = JSON.parse(
            fs.readFileSync(snykOutputFilePath, "utf-8")
          );
          const expectedDetails = `Snyk vulnerability ID: ${issue.id} | Introduced through: goof@1.0.1 › ejs@1.0.0 › something@0.1.0 | Remediation: Pin ejs to version 2.5.3`;

          const details = annotationDetails(issue);

          expect(details).toEqual(expectedDetails);
        });

        it("should Upgrade because from field is less than 2", () => {
          const snykOutputFilePath = `${fixturesBasePath}/snyk_issue_pinnable.json`;
          const issue = JSON.parse(
            fs.readFileSync(snykOutputFilePath, "utf-8")
          );
          const expectedDetails = `Snyk vulnerability ID: ${issue.id} | Introduced through: goof@1.0.1 › ejs@1.0.0 | Remediation: Upgrade ejs to version 2.5.3`;

          const details = annotationDetails(issue);

          expect(details).toEqual(expectedDetails);
        });
      });

      describe("when is Upgradable", () => {
        describe("when is outdated", () => {
          it("is patchable", () => {
            const snykOutputFilePath = `${fixturesBasePath}/snyk_issue_outdated_and_patchable.json`;
            const issue = JSON.parse(
              fs.readFileSync(snykOutputFilePath, "utf-8")
            );
            const expectedDetails = `Snyk vulnerability ID: ${issue.id} | Introduced through: goof@1.0.1 › ejs@1.0.0 | Remediation: Run snyk wizard to patch ejs@1.0.0.`;

            const details = annotationDetails(issue);

            expect(details).toEqual(expectedDetails);
          });

          it("is NOT patchable", () => {
            const snykOutputFilePath = `${fixturesBasePath}/snyk_issue_outdated_not_patchable.json`;
            const issue = JSON.parse(
              fs.readFileSync(snykOutputFilePath, "utf-8")
            );
            const expectedDetails = `Snyk vulnerability ID: ${issue.id} | Introduced through: goof@1.0.1 › ejs@1.0.0 | Remediation: Your dependencies are out of date, otherwise you would be using a newer ejs than ejs@1.0.0. Try reinstalling your dependencies. If the problem persists, one of your dependencies may be bundling outdated modules.`;

            const details = annotationDetails(issue);

            expect(details).toEqual(expectedDetails);
          });
        });

        describe("when is NOT outdated", () => {
          it("is Patchable and supports fix", () => {
            const snykOutputFilePath = `${fixturesBasePath}/snyk_issue_not_outdated.json`;
            const issue = JSON.parse(
              fs.readFileSync(snykOutputFilePath, "utf-8")
            );
            const expectedDetails = `Snyk vulnerability ID: ${issue.id} | Introduced through: goof@1.0.1 › ejs@1.0.0 | Remediation: Upgrade to ejs@2.5.3`;

            const details = annotationDetails(issue);

            expect(details).toEqual(expectedDetails);
          });
        });
      });
    });
  });
});
