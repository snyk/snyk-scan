# Bitbucket Pipelines Pipe: Snyk

This pipe uses Snyk to find, fix and monitor known vulnerabilities in your app dependencies and docker image.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: snyk/snyk-scan:1.0.2
  variables:
    SNYK_TOKEN: "<string>"
    # LANGUAGE: "<string>" # Required unless SNYK_TEST_JSON_INPUT is set
    # IMAGE_NAME: "<string>" # Only required if LANGUAGE set to "docker"
    # SNYK_BASE_IMAGE: "<string>" # Custom base image
    # CODE_INSIGHTS_RESULTS: "<boolean>" # Optional.
    # SNYK_TEST_JSON_INPUT: "<string>" # Optional. The filename or path of a JSON file containg the output from running a test with the Snyk CLI (with json output option).
    # PROTECT: "<boolean>" # Optional.
    # DONT_BREAK_BUILD: "<boolean>" # Optional.
    # MONITOR: "<boolean>" # Optional.
    # SEVERITY_THRESHOLD: "<low|medium|high|critical>" # Optional.
    # ORGANIZATION: "<string>" # Optional.
    # PROJECT_FOLDER: "<string>" # Optional.
    # TARGET_FILE: "<string>" # Optional.
    # EXTRA_ARGS: "<string>" # Optional.
    # DEBUG: "<boolean>" # Optional.
    # SNYK_API: "<string"> # Optional.
```

## Variables

| Variable              | Usage                                     |
| --------------        | ----------------------------------------- |
| SNYK_TOKEN (\*)       | The Snyk API Token. |
| LANGUAGE (\*)         | The development language (e.g. `node`, `ruby`, `composer`, `dotnet` or `docker`). See [Dockerhub](https://hub.docker.com/r/snyk/snyk/tags) for a  full list of possible tags. <br/>**NOTE: When using with SNYK_BASE_IMAGE, this field refers to your base image tag** |
| IMAGE_NAME (\*)       | For `docker` language, the image to do a docker scan on. |
| SNYK_BASE_IMAGE       | Supply your own base image if you do not wish to use [Snyk Images](https://hub.docker.com/r/snyk/snyk) Default: `snyk/snyk`. <br/>**NOTE: `LANGUAGE` will refer to the your base image tag, please ensure the tag is valid** |
| CODE_INSIGHTS_RESULTS | Create Code Insight report with Snyk test results. Default: `false`. |
| SNYK_TEST_JSON_INPUT  | Use if you just want to create a Code Insights report from a previously generated `snyk test --json` output. See example below in the "Generating the Code Insights report from existing Snyk JSON output" section. |
| PROTECT               | This will apply the patches specified in your .snyk file to the local file system. (After running [Snyk Wizard](https://snyk.io/docs/cli-wizard/)) Default: `false`. |
| DONT_BREAK_BUILD      | Continue build despite issues found. Default: `false`. |
| MONITOR               | Record a snapshot of the project to the Snyk UI and keep monitoring it after initial test. Default: `false`. |
| SEVERITY_THRESHOLD    | Reports on issues equal or higher of the provided level. Allowed Values: `low`, `med`, `high`, `critical`. Default: `low`. |
| ORGANIZATION          | Organization to run the cli with. Default: none. |
| PROJECT_FOLDER        | The folder in which the project resides. Default: `.`. |
| TARGET_FILE           | The package file (e.g. `package.json`); equivalent to `--file=` in the CLI. For Docker should point to the `Dockerfile`. Default: none. |
| EXTRA_ARGS            | Extra arguments to be passed to the snyk cli. Default: none. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |
| SNYK_API              | Specifies the Snyk API endpoint. Default: `https://api.snyk.io` (standard Snyk MT-US environment) |


_(\*) = required variable._

## Details

By integrating Snyk into Bitbucket Pipelines you can continuously test, fix and monitor your codebase for known vulnerabilities in your dependencies and docker image.

To find out more about Snyk, head over to [our website](https://snyk.io).

For languages / package managers with more advanced environments such as Java with Maven or Gradle, you should directly use the Snyk CLI in your pipeline. See the "Directly using the Snyk CLI" example. If you also want to generate a Code Insights report, you can feed the the Snyk CLI test output into this Pipe - see the "Generating the Code Insights report from existing Snyk JSON output" example.


## Prerequisites

Snyk API Token is necessary to use this pipe.

- If you don't yet have an account on snyk.io, head over and [register](https://app.snyk.io/signup).
- To obtain a token log in to your Snyk account and obtain an API token from the [account page](https://app.snyk.io/account).
- Add the generated token as a [secured environment variable](https://confluence.atlassian.com/x/0CVbLw#Environmentvariables-Securedvariables) in Bitbucket Pipelines.

## Examples

### Basic app dependencies scan example
Uses Snyk to scan a Node.js application and break the build if any vulnerabilities found.

```yaml
script:
  - npm install

  - npm test

  - pipe: snyk/snyk-scan:1.0.2
    variables:
      SNYK_TOKEN: $SNYK_TOKEN
      LANGUAGE: "node"

  - npm publish
```

### Supplying your own base image
Uses custom supplied Docker Image to run your Snyk Scans. Useful if you do not wish to use the default Snyk Images. The image will take the format `<MY_CUSTOM_IMAGE>:<TAG>`, where `MY_CUSTOM_IMAGE` and `TAG` corresponds to the `SNYK_BASE_IMAGE` and `LANGUAGE` variables respectively.

```yaml
script:
  - docker build -t $IMAGE_NAME .

  - pipe: snyk/snyk-scan:1.0.2
    variables:
      SNYK_TOKEN: $SNYK_TOKEN
      LANGUAGE: "latest"
      SNYK_BASE_IMAGE: $BASE_IMAGE_NAME
      TARGET_FILE: "Dockerfile"

  - docker push $IMAGE_NAME

### Basic Docker image scan example
Uses Snyk to scan a Docker image and break the build if any vulnerabilities found.

```yaml
script:
  - docker build -t $IMAGE_NAME .

  - pipe: snyk/snyk-scan:1.0.2
    variables:
      SNYK_TOKEN: $SNYK_TOKEN
      LANGUAGE: "docker"
      IMAGE_NAME: $IMAGE_NAME
      TARGET_FILE: "Dockerfile"

  - docker push $IMAGE_NAME
```

### Directly using the Snyk CLI
This is especially relavent for those languages which have more complex build environments - for example Java with Maven or Gradle. In these scenarios, you can test your application with the Snyk CLI using some variation of `snyk test`. The easiest way to install the Snyk CLI is with npm (`npm install -g snyk`) but if npm is not available in your build environment, you can download a binary using curl from [https://github.com/snyk/snyk/releases](https://github.com/snyk/snyk/releases).

```yaml
script:
  - mvn install
  - npm install -g snyk # binary download also available
  - snyk test --all-projects

  # rest of your pipeline...
```

### Generating the Code Insights report from existing Snyk JSON output
This is especially relavent for those languages which have more complex build environments - for example Java with Maven or Gradle. In these scenarios, you can test your application with the Snyk CLI using some variation of `snyk test --json > snyk-test-output.json` and then feed the output file forward into the Pipe to generate a Code Insights report. The easiest way to install the Snyk CLI is with npm (`npm install -g snyk`) but if npm is not available in your build environment, you can download a binary using curl from [https://github.com/snyk/snyk/releases](https://github.com/snyk/snyk/releases).

```yaml
script:
  - mvn install
  - npm install -g snyk # binary download also available
  - snyk test --all-projects --json > snyk-test-output.json

  - pipe: snyk/snyk-scan:1.0.2
    variables:
      SNYK_TOKEN: $SNYK_TOKEN
      SNYK_TEST_JSON_INPUT: "snyk-test-output.json"

  # rest of your pipeline...
```

### Advanced example
Apply Snyk patches to a Node.js app, and continue to scan the dependencies for *critical severity* vulnerabilities only, but not failing the build if found any. Also opt-in to monitor the dependencies states on Snyk.io to get alerts if new vulns found. Then go on to build the Docker image and scan it for *critical severity* vulnerabilities.

```yaml
script:
  - npm install

  - npm test

  - pipe: snyk/snyk-scan:1.0.2
    variables:
      SNYK_TOKEN: $SNYK_TOKEN
      LANGUAGE: "node"
      PROTECT: "true"
      SEVERITY_THRESHOLD: "critical"
      DONT_BREAK_BUILD: "true"
      MONITOR: "true"

  - docker build -t $IMAGE_NAME .

  - pipe: snyk/snyk-scan:1.0.2
    variables:
      SNYK_TOKEN: $SNYK_TOKEN
      LANGUAGE: "docker"
      IMAGE_NAME: $IMAGE_NAME
      TARGET_FILE: "Dockerfile"
      SEVERITY_THRESHOLD: "high"
      DONT_BREAK_BUILD: "true"
      MONITOR: "true"

  - docker push $IMAGE_NAME

```

## Scanning using against a custom API endpoint 

For customers using non-default instances of Snyk, the usage of `SNYK_API` is required to specify the URL. This applies to MT-EU, MT-AU and single tenant customers.


```yaml
script:
  - npm install

  - npm test

  - pipe: snyk/snyk-scan:1.0.2
    variables:
      SNYK_TOKEN: $SNYK_TOKEN
      SNYK_API: https://api.eu.snyk.io
      LANGUAGE: "node"

  - npm publish
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, please contact us at: [support@snyk.io](support@snyk.io).

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2019 Snyk Ltd. and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
